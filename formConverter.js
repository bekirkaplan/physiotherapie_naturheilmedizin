function camelize(str) {
    return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(word, index) {
        return index === 0 ? word.toLowerCase() : word.toUpperCase();
    }).replace(/\s+/g, '');
}

var slugify = function(text) {
    var trMap = {
        'çÇ':'c',
        'ğĞ':'g',
        'şŞ':'s',
        'üÜ':'u',
        'ıİ':'i',
        'öÖ':'o',
        'ä': 'ae',
        'ß' : 'ss'
    };
    for(var key in trMap) {
        text = text.replace(new RegExp('['+key+']','g'), trMap[key]);
    }
    return  camelize(text.replace(/[^-a-zA-Z0-9\s]+/ig, '') // remove non-alphanumeric chars
        .replace(/\s/gi, " ") // convert spaces to dashes
        .replace(/[-]+/gi, " ")); // trim repeated dashes


}

var getAutoGenerateName = function(text, prefix) {
    var autoGenName = slugify(text);
    return prefix ? prefix + '.' + autoGenName.slice(0, 40) : autoGenName.slice(0, 40);
}

var getFormAsAdenTemplate = function(element, prefix='') {
    var arr = []
    element.querySelectorAll('input').forEach(e=>{
        switch (e.type) {
            case 'checkbox':
                arr.push({
                    "type": e.type,
                    "name": getAutoGenerateName(e.value, prefix),
                    "value": e.value,
                    "label": e.value,
                    "style": {
                        "className": "col-xs-12"
                    }
                });
                break;
            case 'text':
                arr.push({
                    "type": e.type,
                    "name": getAutoGenerateName(e.name, prefix),
                    "value": e.value,
                    "label": e.value,
                    "style": {
                        "className": "col-xs-12"
                    }
                });
                break;
        }

    });
    return JSON.stringify(arr, undefined, 4);
}
