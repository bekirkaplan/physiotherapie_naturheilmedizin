const restify = require('restify');
const mkdirp = require('mkdirp');
const fs = require('fs-extra');
const sjcl = require('sjcl');
const MongoClient = require('mongodb').MongoClient;
const initialData = require('./init');
const corsMiddleware = require('restify-cors-middleware');
const mode = 'production'; // production, development
const mime = require('mime-types');
const ENUMS = require("./helpers/enums");

const dbName = initialData.dbs.physiotherapie;
const serverPort = initialData.ports.physiotherapie;
var indexArr = {};
var errs = require('restify-errors');
/*
 jwt
 */

const config = require('./config');
const user = require('./lib/user');
const Sequence = require('./lib/sequence');
const mailer = require('./lib/mailer');
const rjwt = require('restify-jwt-community');
const jwt = require('jsonwebtoken');
const xXssProtection = require("x-xss-protection");
const sequence =new Sequence();


/* Pasword reset */


const generateRandomPass = function () {
    const pass = Math.random().toString(36).slice(-8);
    const hashPass = sjcl.codec.hex.fromBits(sjcl.hash.sha256.hash(pass));
    return {
        newpass: pass,
        newhash: hashPass
    };
}

MongoClient.connect(config.mongourl, {useNewUrlParser: true}, function (err, client) {
    const db = client.db(dbName);
    sequence.createSequenceTable(db);

    function respond(req, res, next) {
        res.send({success: true});
        next();
    }


   // const seq1 = sequence.createSequenceRecord("users",db);
    //const seq2 = sequence.getNextSequenceValue("users",db);

    async function getSeq(req, res, next) {
        const jsonError = {};
        try {
            const {body} = req;
            const tableName = body.tableName;
            const metod = body.metod;
            if (metod=='nextVal')
            {

                sequence.getNextSequenceValue(tableName, db, function(result) {
                    if(result.success) {
                        res.send({success: true, data: result});
                    } else {
                        res.send({success: false, data: result});
                    }
                });

            }
            if (metod=='create')
            {
                sequence.createSequenceRecord(tableName, db, function(result) {
                    if(result.success) {
                        res.send({success: true, data: result});
                    } else {
                        res.send({success: false, data: result});
                    }
                });

            }
            if (metod=='current')
            {
                sequence.currentSequenceRecord(tableName, db, function(result) {
                    if(result.success) {
                        res.send({success: true, data: result});
                    } else {
                        res.send({success: false, data: result});
                    }
                });

            }

        } catch (ex) {
            console.log(ex);
            jsonError.failure = true;
            jsonError.error = ex.message;
            res.send({success: false, data: jsonError});
        }
    }


    async function forgotpassword(req, res, next) {
        let {mailadress} = req.body;

        const jsonResObj = {};
        try {
            const {query} = req;
            const tableName = 'users';
            const collection = db.collection(tableName);
            const column = "username";
            const value = mailadress;
            const queryWhere = {"active": {"$ne": false}};

            queryWhere[column] = value;

            await collection.find(JSON.parse(queryWhere)).toArray((err, result) => {
                if (err) {
                    console.log(err);
                    jsonResObj.failure = err;
                    res.send({success: false, data: jsonResObj});
                }
                if (result) {

                    let passobj = generateRandomPass();
                    const singleData = result[0];
                    if (singleData !== undefined) {
                        singleData.password = passobj.newhash.map(String);

                        let recordedData;
                        if (singleData.id) {
                            recordedData = collection.update({"id": singleData.id}, singleData, {
                                upsert: true
                            });
                        }
                    }
                    mailer.sendMail("***@gmail.com", mailadress,passobj.newpass,function (rss) {
                        if (rss)
                        {
                            jsonResObj.data = "Mail Gönderilmiştir.";
                        }
                    });
                }
                jsonResObj.success = true;
            });


          //  next();

        } catch (ex) {
            console.log(ex);
            jsonResObj.failure = ex.message;
            res.send({success: false, data: jsonResObj});
        }


        // mailer.sendMail("seragir@gmail.com", mailAdress);
        res.send({success: true});
        next();
    }




    const GEN_UID = function() {

        var lut = [];
        for (var i = 0; i < 256; i++) {
            lut[i] = (i < 16 ? '0' : '') + i.toString(16);
        }

        var d0 = (Math.random() * 0xffffffff) | 0;
        var d1 = (Math.random() * 0xffffffff) | 0;
        var d2 = (Math.random() * 0xffffffff) | 0;
        var d3 = (Math.random() * 0xffffffff) | 0;
        return (
            lut[d0 & 0xff] +
            lut[(d0 >> 8) & 0xff] +
            lut[(d0 >> 16) & 0xff] +
            lut[(d0 >> 24) & 0xff] +
            '-' +
            lut[d1 & 0xff] +
            lut[(d1 >> 8) & 0xff] +
            '-' +
            lut[((d1 >> 16) & 0x0f) | 0x40] +
            lut[(d1 >> 24) & 0xff] +
            '-' +
            lut[(d2 & 0x3f) | 0x80] +
            lut[(d2 >> 8) & 0xff] +
            '-' +
            lut[(d2 >> 16) & 0xff] +
            lut[(d2 >> 24) & 0xff] +
            lut[d3 & 0xff] +
            lut[(d3 >> 8) & 0xff] +
            lut[(d3 >> 16) & 0xff] +
            lut[(d3 >> 24) & 0xff]
        );
    };

    async function registration(req, res, next) {
        const jsonError = {};
        try {
            const {body} = req;
            const tableName = "users";
            const collection = db.collection(tableName);
            const uuid = GEN_UID();
            let realUser = {
                id: uuid,
                admin: false,
                state: ENUMS.USER_STATE.PENDING,
                name: body.name,
                surname: body.surname,
                username: body.mailadress,
                password: body.password,
                createdDate: new Date().getTime()
            };

            collection.find({username: body.mailadress}).toArray((err, data) => {
                if (err) return reject(err);
                if(data.length > 0) {
                    res.send({success: false, message: "User already exist."});
                } else {
                    saveOrUpdateDb(collection, realUser).then((saveResult) => {
                        console.log(saveResult);
                        res.send({success:true, data: "Registration successfull.."});
                    });
                }
            });


        } catch (ex) {
            console.log(ex);
            jsonError.failure = true;
            jsonError.error = ex.message;
            res.send({success: false, data: jsonError});
        }
    }

    async function createIndex(collection) {
        try {
            if (indexArr[collection.collectionName + ".id"] == undefined) {
                await collection.createIndex({"id": 1}, {unique: true, name: "id"}, (call) => {
                    console.log("create index " + collection.collectionName + ".id");
                    indexArr[collection.collectionName + ".id"] = collection.collectionName + ".id";
                });
            }
        } catch (ex) {
            console.log("createIndex eroor:" + ex.message);
        }
    }

    function strictChecker(req, res, next) {
        if (!initialData.strict) {
            return next();
        }

        const collections = Object.keys(req.body);
        const body = {};

        collections.forEach((collection) => {
            const strictCol = initialData.collections[collection];
            if (strictCol) {
                body[collection] = Object.assign({}, req.body[collection], {data: []});
                req.body[collection].data.forEach((data) => {
                    const newData = {};
                    Object.keys(data).forEach((key) => {
                        if (strictCol[key] || key === '_id') {
                            newData[key] = data[key];
                        }
                    });
                    body[collection].data.push(newData);
                });
            }
        });

        req.body = body;
        next();
    }

    async function deleteById(req, res, next) {
        const jsonError = {};
        try {
            const {body} = req;
            const keys = Object.keys(body);
            let jsonObj = {
                tableName: '',
                data: []
            };
            let jsonArr = [];

            for (let i = 0; i < keys.length; i += 1) {
                const collectionName = keys[i];
                const {data} = body[collectionName];
                const collection = db.collection(collectionName);
                jsonObj = {
                    tableName: collectionName,
                    data: []
                };

                for (let j = 0; j < data.length; j += 1) {
                    let singleData = data[j];
                    singleData.active = false;
                    let recordedData;

                    if (singleData.id) {
                        recordedData = await collection.update({"id": singleData.id}, singleData, {
                            upsert: true // insert the document if it does not exist
                        });
                        delete singleData.active;
                    }
                    jsonObj.data.push(singleData);
                }
                jsonArr.push(jsonObj);
            }
            res.send({success: true, data: jsonArr});

        } catch (ex) {
            console.log(ex);
            jsonError.failure = true;
            jsonError.error = ex.message;
            res.send({success: false, data: jsonError});
        }
    }


    async function getFile(req, res, next) {
        const {body} = req;

        let filePath = `./public/uploads/${body.path}`;
        let mimeType = mime.lookup(filePath);

        fs.readFile(filePath, function(err, file) {
            if (err) {
                res.send(500);
                return res.end();
            }
            res.send( { success: true, data: file, mime: mimeType, fileName: body.name } );
        });

    };

    async function removeFile(req, res, next) {
        const {body} = req;

        let filePath = `./public/uploads/${body.path}`;
        let mimeType = mime.lookup(filePath);

        try {
            fs.unlink(filePath, function(err, file) {

                const collection = db.collection(body.dbtable);
                saveOrUpdateDb(collection, body.data);

                if (err) {
                    res.send(500);
                    return res.end();
                }
                res.send( { success: true, data: file, mime: mimeType, fileName: body.name } );
            });
        } catch(err) {
            console.error(err)
        }


    };

    async function fileupload(req, res, next) {

        const jsonError = {};
        let fileObj = {};
        let filePrefix = "";
        try {
            sequence.createSequenceRecord('fileuploads', db);
            mkdirp(`${__dirname}/public/uploads/${req.params.directory}`, function(err) {
                if(!err) {
                    for(let key in req.files) {
                        filePrefix = '';
                        sequence.getNextSequenceValue('fileuploads', db, (seqVal)=>{
                            if(req.files.hasOwnProperty(key)) {
                                fileObj[key] = [];
                                fs.move(req.files[key].path, `${__dirname}/public/uploads/${req.params.directory}${filePrefix + seqVal.sequence_value + '_' + req.files[key].name}`, { overwrite: false });
                                req.files[key].name = seqVal.sequence_value + '_' + req.files[key].name;
                                fileObj[key].push(req.files[key]);
                            }
                            res.send({success: true, data: fileObj});
                        });
                    }
                } else {
                    res.send({success: false, data: err});
                }

            });

        } catch (ex) {
            console.log(ex);
            jsonError.failure = true;
            jsonError.error = ex.message;
            res.send({success: false, data: jsonError});
        }
    }

    async function deleteAll(req, res, next) {
        const jsonError = {};
        try {
            const {body} = req;
            const keys = Object.keys(body);
            let jsonObj = {
                tableName: '',
                data: []
            };
            let jsonArr = [];

            for (let i = 0; i < keys.length; i += 1) {
                const collectionName = keys[i];
                const {data} = body[collectionName];
                const collection = db.collection(collectionName);
                jsonObj = {
                    tableName: collectionName,
                    data: []
                };

                for (let j = 0; j < data.length; j += 1) {
                    let singleData = data[j];
                    singleData.active = false;
                    let recordedData;

                    if (singleData.id) {
                        recordedData = await collection.update({"id": singleData.id}, singleData, {
                            upsert: true // insert the document if it does not exist
                        });
                        delete singleData.active;
                    }
                    jsonObj.data.push(singleData);
                }
                jsonArr.push(jsonObj);
            }
            res.send({success: true, data: jsonArr});

        } catch (ex) {
            console.log(ex);
            res.send({success: false, data: ex.message});
        }
    }

    async function getById(req, res, next) {
        const jsonError = {};
        try {
            const {body} = req;
            const tableName = body.tableName;
            const id = body.id;
            const collection = db.collection(tableName);
            if (Boolean(req.user.realUser.admin) !== true) {
                await collection.find({
                    id: id,
                    active: {$ne: false},
                    userId: {$eq: req.user.realUser.id}
                }).toArray(function (err, result) {
                    if (result) {
                        res.send({success: true, data: result});
                    }
                    if (err) {
                        res.send({success: true, data: err});
                    }
                });
            } else {
                await collection.find({
                    id: id,
                    active: {$ne: false}
                }).toArray(function (err, result) {
                    if (result) {
                        res.send({success: true, data: result});
                    }
                    if (err) {
                        res.send({success: false, data: err});
                    }
                });
            }
            next();
        } catch (ex) {
            console.log(ex);
            res.send({success: false, data: ex.message});
        }
    }


    async function getAll(req, res, next) {
        const jsonResObj = {};
        try {
            const {body} = req;
            const tableName = body.tableName;
            const collection = db.collection(tableName);
            /*if (Boolean(req.user.realUser.admin) !== true) {

                await collection.find({$and: [{userId: {$eq: req.user.realUser.id}}, {active: {$ne: false}}]}).toArray(function (err, result) {
                    if (err) {
                        console.log(err);
                        jsonResObj.failure = err;
                        res.send({success: false, data: jsonResObj});
                    }
                    if (result) {
                        result.forEach(element => {
                            delete element._id;
                        });
                        jsonResObj.success = true;
                        jsonResObj.data = result;
                    }
                    res.send({success: true, data: jsonResObj});
                });

            } else {*/
                let queryArr = [];
                let realQuery = {active: {$ne: false}};

                if(req.user.realUser.id && !(Boolean(req.user.realUser.admin) === true)) {
                    realQuery = {$and: [{userId: {$eq: req.user.realUser.id}}, {active: {$ne: false}}]};
                }


                await collection.find(realQuery).toArray(function (err, result) {
                    if (err) {
                        console.log(err);
                        jsonResObj.success = false;
                        jsonResObj.data = err;
                        res.send({success: false, data: jsonResObj});
                    }
                    if (result) {
                        result.forEach(element => {
                            delete element._id;
                        });
                        jsonResObj.success = true;
                        jsonResObj.data = result;
                    }
                    res.send(jsonResObj);
                });
            /*}*/
            next();

        } catch (ex) {
            res.send({success: false, data: ex.message});
        }
    }

    async function getByValue(req, res, next) {
        const jsonResObj = {};
        try {
            const {body} = req;
            const tableName = body.tableName;
            const collection = db.collection(tableName);
            const column = body.key;
            const value = body.value;
            const query = body.query;
            let queryWhere = {active: {$ne: false}};
            if(query) {
                queryWhere = Object.assign(queryWhere, query);
            } else {
                queryWhere[column] = value;
            }

            await collection.find(queryWhere).toArray(function (err, result) {
                if (err) {
                    res.send({success: false, data: err});
                }
                if (result) {
                    result.forEach(element => {
                        delete element._id;
                    });
                    res.send({success: true, data: result});
                }
            });
            next();

        } catch (ex) {
            res.send({success: false, data: ex.message});
        }
    }



    async function saveOrUpdateDb(collection, singleData) {
        return collection.update({"id": singleData.id}, singleData, {
            upsert: true // insert the document if it does not exist
        });
    }
    async function saveOrUpdate(req, res, next) {
        const jsonError = {};
        let sequenceDataObj = {};
        try {
            const {body} = req;
            let reqData = body;
            if(typeof body === "string") {
                reqData = JSON.parse(reqData);
            }
            const keys = Object.keys(reqData);
            let jsonObj = {
                tableName: '',
                data: []
            };
            let jsonArr = [];
            for (let i = 0; i < keys.length; i += 1) {
                const collectionName = keys[i];
                const {data} = reqData[collectionName];
                const collection = db.collection(collectionName);

                createIndex(collection);

                jsonObj = {
                    tableName: collectionName,
                    data: []
                };
                for (let j = 0; j < data.length; j += 1) {

                    // serverside session control yapilmasi lazım
                    // req.user dan alınan user id farklı gonderilirse
                    // nasıl kontrol edecegiz dogru user olduguna bakmaliyiz.
                    if (data[j].userId==undefined)
                        data[j].userId = req.user.realUser.id;

                    if(data[j].hasOwnProperty('sequences')) {
                        sequenceDataObj = await sequence.formSequences(data[j].sequences);
                        data[j].sequences = sequenceDataObj;
                    }

                    let singleData = data[j];

                    if (singleData.id) {
                        saveOrUpdateDb(collection, singleData);
                    }
                    jsonObj.data.push(singleData);
                }
                jsonArr.push(jsonObj);
                //mailer.nodemailer.sendMail();

            }
            res.send({success: true, data: jsonArr});
        } catch (ex) {
            console.log(ex);
            jsonError.failure = true;
            jsonError.error = ex.message;
            res.send({success: false, data: jsonResObj});
        }

    }



    async function getReports(req, res, next) {
        const jsonResObj = {};
        try {
            const {
                body
            } = req;
            const tableName = body.tableName;
            const subTables = body.subTables;

            const collection = db.collection(tableName);

            if (Boolean(req.user.realUser.admin) === true) {

                let queryWhereSub = [
                    {
                        '$addFields': {
                            'active': true
                        }
                    }
                ];


                subTables.forEach((subTable) => {
                    queryWhereSub.push({
                        '$lookup': {
                            'from': subTable.table,
                            'localField': 'id',
                            'foreignField': subTable.fk,
                            'as': subTable.table
                        }
                    });
                });

                await collection.aggregate(queryWhereSub).toArray(function (err, result) {
                    if (err) {
                        console.log(err);
                        jsonResObj.success = false;
                        jsonResObj.data = err;
                        res.send({
                            success: false,
                            data: jsonResObj
                        });
                    }
                    if (result) {

                        result.forEach(element => {
                            delete element._id;
                        });
                        res.send({
                            success: true,
                            data: result
                        });
                    }

                });
            }


            next();

        } catch (ex) {
            res.send({
                success: false,
                data: ex.message
            });
        }
    }


    const server = restify.createServer();
    const cors = corsMiddleware({
        preflightMaxAge: 5, //Optional
        origins: ['*'],
        allowHeaders: ['Authorization']
    });

    server.pre(cors.preflight);
    server.use(cors.actual);

    server.use(restify.plugins.acceptParser(server.acceptable));
    server.use(restify.plugins.dateParser());
    server.use(restify.plugins.queryParser());
    server.use(restify.plugins.bodyParser({
        maxBodySize: 100 * 4096,
        mapParms: false,
        mapFiles: true,
        keepExtensions: true,
        multiples: true
    }));
    server.use(xXssProtection());

    // uploadDir: './public/uploads'


    server.get('/_utils', respond);
    server.post('/getSeq', getSeq);
    server.post('/getAll', getAll);
    server.post('/getByValue', getByValue);
    server.post('/getById', getById);
    server.post('/getFile', getFile);
    server.post('/getReports', getReports);
    server.post('/removeFile', removeFile);
    server.post('/deleteById', deleteById);
    server.post('/deleteAll', deleteAll);
    server.post('/forgotpassword', forgotpassword);
    server.post('/registration', registration);

    server.post('/fileupload', fileupload);
    server.post('/saveOrUpdate', [
        strictChecker,
        saveOrUpdate
    ]);

    server.use(rjwt(config.jwt).unless({
        path: ['/auth', '/forgotpassword', '/registration']
    }));

    // // using the `req.user` object provided by restify-jwt
    server.get('/user', (req, res, next) => {
        res.send({success: true, user: req.user});
    });
    server.post('/auth', (req, res, next) => {
        let {username, password} = req.body;
        user.authenticate(db, username, password).then(user => {
            // creating jsonwebtoken using the secret from config.json
            if (user.length > 0 && mode === 'production') {

                let realUser = user[0];

                /* login olan kullanıcının json bilgileri */
                this.authUser = realUser;

                let token = jwt.sign({realUser}, config.jwt.secret, {
                    expiresIn: 21600 // 604800 1 haftalik expire
                });
                // retrieve issue and expiration times
                let {iat, exp} = jwt.decode(token);
                delete realUser._id;
                res.send({iat, exp, token, user: realUser, success: true});
            } else if (mode === 'development') {
                let realUser = {
                    admin: true,
                    name: 'admin',
                    password: password,
                    username: 'admin@admin.com',
                    surname: 'admin'
                };
                let token = jwt.sign({realUser}, config.jwt.secret, {
                    expiresIn: 604800 // 1 haftalik expire
                });
                // retrieve issue and expiration times
                let {iat, exp} = jwt.decode(token);
                delete realUser._id;
                res.send({iat, exp, token, user: realUser, success: true});
            } else {
                let jsonResObj = {};
                jsonResObj.failure = `User name ${username} could not found!!!`;
                jsonResObj.success = false;
                res.send(jsonResObj);
            }

        }).catch(function (error) {
            console.log(error);
            let jsonResObj = {};
            jsonResObj.failure = error.message;
            jsonResObj.success = false;
            res.send(jsonResObj);
        });
    });

    server.listen(serverPort, function () {
        console.log('%s listening at %s', server.name, server.url);
    });

    server.get('/', function (req, res, next) {
        return next(new errs.InternalServerError('boom!'));
    });


    server.on('restifyError', function (req, res, err, callback) {
        err.toJSON = function customToJSON() {
            return {
                success: false,
                name: err.name,
                message: err.message
            };
        };
        err.toString = function customToString() {
            return 'i just want a string';
        };


        res.send(err.toJSON());
        return callback();
    });


});
