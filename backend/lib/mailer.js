"use strict";
const nodemailer = require ('nodemailer'),
    credentials=require('./credentials.js');

var mailTransport=nodemailer.createTransport({
    service:'Gmail',
    auth: {
        user : credentials.user,
        pass : credentials.password,
    }
});

 async function sendMail(fromMailAdress,toMailAdress,pass,calback){
     debugger;
      mailTransport.sendMail({
        from: ' "AdenJS" <fromMailAdress>',
        to : toMailAdress,
        subject : 'Hello AdenJS',
        text: "Şifre Sıfırlama, yeni şifreniz ["+pass+"]",
    },function(err,info){
        if(err){
            console.log('Unable to send the mail :'+err.message);
            calback(false);
        }
        else{
            console.log('Message response : '+info.response);
            calback(true);
        }
    });
     //calback(false);
};

exports.sendMail=sendMail;