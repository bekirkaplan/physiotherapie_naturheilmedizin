/**
 * Created by pc on 07.04.2019.
 */
const config = require('../config');
const MongoClient = require('mongodb').MongoClient;
const initialData = require('../init');
const ENUMS = require("../helpers/enums");
// exports.authenticate = (username, password) => {
//     return Promise.resolve({ uid: 1, name: 'Sean', admin: false });
// };
exports.authenticate = (db, username1, password1) => {
    return new Promise((resolve, reject) => {
        const collection = db.collection('users');
        collection.find({username: username1,password:password1, state: ENUMS.USER_STATE.ACTIVE}).toArray(function (err, data) {
            if (err) return reject(err);
            resolve(data);
        });
    });
};
