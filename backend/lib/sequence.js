/**
 * Created by pc on 11.05.2019.
 */

"use strict";

const initialData = require('../init');
const sequenceTableName = "counter";

class Sequence {
    constructor() {
    }

    createSequenceTable(db) {
        const collection = db.collection(sequenceTableName);
        db.createCollection(sequenceTableName)
    }

    createSequenceRecord(sequenceName, db, callback) {
        const collection = db.collection(sequenceTableName);
        let singleData = {name: sequenceName, sequence_value: 0};
        collection.find({
            name: sequenceName
        }).toArray(function (err, result) {
            
            if (err) {
                if(callback && callback.constructor.name == 'AsyncFunction') {
                    callback(false);
                }
            } else {
                if (result.length == 0) {
                    collection.update({"name": singleData.name}, singleData, {
                        upsert: true // insert the document if it does not exist
                    });
                }
                if(callback && callback.constructor.name == 'AsyncFunction') {
                    callback(true);
                }
            }
        });
    }
    
    async currentSequenceRecord(sequenceName, db, callback) {
        const collection = db.collection(sequenceTableName);
        var sequenceDocument = await collection.findAndModify(
            {
                name: sequenceName
            },
            [],
            {$inc: {sequence_value: 0}},
            {
                new: true
            },
            function (err, result) { //callback
                if (err) {
                    throw err
                } else {
                    callback(result.value);
                }
            }
        );
    }

    resetSequenceTable(tableName, db) {
        const collection = db.collection(sequenceTableName);
        let singleData = {name: tableName, sequence_value: 0};
        collection.update({"name": singleData.name}, singleData, {
            upsert: true // insert the document if it does not exist
        });
    }

    async getNextSequenceValue(sequenceName, db, callback) {

        const collection = db.collection(sequenceTableName);

        this.createSequenceRecord(sequenceName, db, async (res) => {
            if(res) {
                await collection.findAndModify(
                    {
                        name: sequenceName
                    },
                    [],
                    {$inc: {sequence_value: 1}},
                    {
                        new: true
                    },
                    function (err, result) { //callback
                        if (err) {
                            throw err
                        } else {
                            result.success = true;
                            callback(result);
                        }
                    }
                );
            }            
        });
        

    }
    async formSequences(sequencesObj) {
        console.log(sequencesObj);
        console.log(Object.keys(sequencesObj));

        

        return sequencesObj;
    }


}

module.exports = Sequence;