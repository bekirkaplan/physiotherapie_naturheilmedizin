module.exports = {
    strict: false,
    dbs: {
        physiotherapie: 'physiotherapie'
    }, // AdenJs
    ports: {
        physiotherapie: 9097
    },
    collections: null,
};
