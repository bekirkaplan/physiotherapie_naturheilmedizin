function index(){


	goc.flydb.interface.dropTable('SCHMERZ');
	goc.flydb.createStructureFromEntities();

	goc.flydb.interface.saveOrUpdate('SCHMERZ', [
		{id:1, value:0},
		{id:2, value:1},
		{id:3, value:2},
		{id:4, value:3},
		{id:5, value:4},
		{id:6, value:5},
		{id:7, value:6},
		{id:8, value:7},
		{id:9, value:8},
	    {id:10, value:9},
		{id:11, value:10}
	]);


}
index.prototype = {
	init: function() {
		goc.page.load({
			path:'languages/',
			pageName: 'language.tr',
			type:"json"
		});
		goc.page.load({
			path:'dashboards/main/',
			domId:'content',
			pageName: 'main'
		});
		$('._kullaniciAdi').html(goc.scd.user.name + ' ' + goc.scd.user.surname);
		if(goc.scd.user.admin == "") {
			$('#adminMenu').remove();
		}
	}
}

window['index'] = new index();
