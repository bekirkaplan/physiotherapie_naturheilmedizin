/**
 * Created by SHOGUN on 11/11/2018.
 */
function patientList() {};

patientList.prototype = {
    init: function () {
        console.log("patientList loaded...");
    },
    ready: function () {
        patientList.hastaNo = 1;
    },
    addPatient: function () {
        goc.page.load({
            path: 'components/addPatient/',
            domId: 'mainContent',
            pageName: 'addPatient',
            animation: {
                type: 'slideDown',
                speed: 500
            },
            type: "json"
        });
    },
    openShmerzen: function (id, relevanz) {
        goc.page.load({
            path: `components/${relevanz}/`,
            domId: 'mainContent',
            pageName: `${relevanz}`,
            data: {
                patientId: id
            },
            animation: {
                type: 'slideDown',
                speed: 500
            },
            type: "json"
        });
    },
    selectedPatient: function (data) {
        if (data.length > 0) {
            $('#duzenleBtn').attr("disabled", false);
            $('#previousTreatmentBtn').attr("disabled", false);
            $('#klinikDurumBtn').attr("disabled", false);
        } else {
            $('#duzenleBtn').attr("disabled", true);
            $('#previousTreatmentBtn').attr("disabled", true);
            $('#klinikDurumBtn').attr("disabled", true);
        }
    },
    gerceklesmeHesapla: function(data) {
        var gerceklesme = 0;
        if (data && data.count !== 0) {
            gerceklesme = (data.filled / data.count) *  100;
        }
        return gerceklesme.toFixed(0);
    },
    getButton: function(row, label, clazz) {
        return goc.formitems.getFormElement({
            "type": 'button',
            "label": label,
            "onclick": 'openShmerzen("' + row.id + '", "' + clazz + '")',
            "style": {
                "className": "btn-success col-xs-12"
            }
        });
    },
    editPatientRenderer: function(data, type, row, meta) {
        // let gerceklesme = patientList.gerceklesmeHesapla(row._count_hasta_form);
        return patientList.getButton(row, 'Anordnung', 'addPatient');
    },
    shmerzenARenderer: function(data, type, row, meta) {
        return patientList.getButton(row, 'A-Relevanz', 'schmerzen_arelevanz');
    },
    shmerzenBRenderer: function(data, type, row, meta) {
        return patientList.getButton(row, 'B-Relevanz', 'schmerzen_brelevanz');
    },
    erkrankungenSymptomeRenderer: function(data, type, row, meta) {
        return patientList.getButton(row, '4a. Erkrankungen bzw. Symptome meines Körpers', 'erkrankungenSymptome');
    },
    allgemeineFragenZuDenSymptomenRenderer: function(data, type, row, meta) {
        return patientList.getButton(row, '4b. Allgemeine Fragen zu den Symptomen meines Körpers', 'allgemeineFragenZuDenSymptomen');
    },
    operationenRenderer: function(data, type, row, meta) {
        return patientList.getButton(row, '5. Operationen', 'operationen');
    },
    medikamenteUndNahrungsergaenzungenRenderer: function(data, type, row, meta) {
        return patientList.getButton(row, '6. Medikamente und Nahrungsergänzungen', 'medikamenteUndNahrungsergaenzungen');
    },
    fragenZumStuhlUndUrinRenderer: function(data, type, row, meta) {
        return patientList.getButton(row, '7. Fragen zum Stuhl und Urin', 'fragenZumStuhlUndUrin');
    },
    ernaehrungTrinkverhaltenGenussmittel: function(data, type, row, meta) {
        return patientList.getButton(row, '8. Ernährung , Trinkverhalten, Genussmittel', 'ernaehrungTrinkverhaltenGenussmittel');
    },
    fragenZumGemutszustandEmotionenPsyche: function(data, type, row, meta) {
        return patientList.getButton(row, '9. Fragen zum Gemütszustand, Emotionen, Psyche', 'fragenZumGemutszustandEmotionenPsyche');
    },
    familienanamnese: function(data, type, row, meta) {
        return patientList.getButton(row, '10. Familienanamnese', 'familienanamnese');
    },
    laborwerte: function(data, type, row, meta) {
        return patientList.getButton(row, '11. Laborwerte', 'laborwerte');
    }
}
window['patientList'] = new patientList();
