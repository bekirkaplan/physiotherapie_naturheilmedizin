function fragenZumStuhlUndUrin() {
    this.defaultFormValues = {
        "aStuhlfarbe": {
            "tiefbraunschwarz": true,
            "grauLehmfarbencremfarben": false,
            "weissGrauWeiss": false,
            "ocker": false,
            "grun": false,
            "rot": false,
            "blutImStuhl": false
        },
        "bStuhlkonsistenz": {
            "typ1EinzelneHarteNussgrosseKugelchen": true,
            "typ2WurstartigKlumpigTyp2WurstartigKlumpig": false,
            "typ3WurstartigMitRissigerOberflaeche": false,
            "typ4WurstartigMitGlatterOberflaeche": false,
            "typ5EinzelneWeicheGlattrandigeKlumpchen": false,
            "typ6EinzelneWeicheKlumpchenMitAusgefranstemRand": false,
            "typ7WaessrigOhneFesteBestandteile": false,
            "bleistiftstuhl": false
        },
        "haeufigkeitDesStuhlgangs": {"alle2428Stunden": true, "23XWoche": false, "mehrAls3XTaeglich": false},
        "cUrin": {
            "ichLasseMehrAls2LtrUrinTaeglich": true,
            "ichHabeHaeufigenHarndrang": false,
            "ichLasseHaeufigNachtsUrin": false,
            "ichLasaseSewhrHaeufigWasserMitNichtGesteigertenUrinm": false
        },
        "wieIstFarbeDesHarn": {"orangeBisBraun": true, "baunBisSchwarz": false, "intensivGelb": false, "rot": false},
        "konsistenzDesUrins": {
            "meinUrinIstMeistensTrub": true,
            "meinUrinIstOftSchaumig": false,
            "meinUrinWeisstOftEinenSusslichenGeruchAuf": false
        }
    };
}

fragenZumStuhlUndUrin.prototype = {
    init: function () {

    },
    ready: function () {
        goc.flydb.interface.get('FRAGEN_ZUM_STUHL_UND_URIN').byValue('patientId', fragenZumStuhlUndUrin._data.patientId).exec((res) => {
            if (res.success && res.data.length > 0) {
                fragenZumStuhlUndUrin.form.fragenZumStuhlUndUrinForm.setValues(res.data[0]);
            } else {
                fragenZumStuhlUndUrin.form.fragenZumStuhlUndUrinForm.setValues(fragenZumStuhlUndUrin.defaultFormValues);
            }
        });
    },
    saveOrUpdate: function (data) {
        if (!fragenZumStuhlUndUrin._data.patientId) {
            goc.notifications.error('Patient not selected', 'Please select a patient from patient list page.');
            return;
        }

        data.patientId = fragenZumStuhlUndUrin._data.patientId;
        goc.flydb.interface.saveOrUpdate('FRAGEN_ZUM_STUHL_UND_URIN', data, (res) => {
            if (res.success) {
                goc.notifications.success('Success', 'Patient data saved successfuly.');
                goc.page.load({
                    path: `components/ernaehrungTrinkverhaltenGenussmittel/`,
                    domId: 'mainContent',
                    data: {
                        patientId: fragenZumStuhlUndUrin._data.patientId
                    },
                    pageName: `ernaehrungTrinkverhaltenGenussmittel`,
                    animation: {
                        type: 'slideDown',
                        speed: 500
                    },
                    type: "json"
                });
            }
        });
    },
    dismiss: function () {
        goc.page.load({
            path: `components/patientList/`,
            domId: 'mainContent',
            pageName: `patientList`,
            animation: {
                type: 'slideDown',
                speed: 500
            },
            type: "json"
        });
    }
}

window['fragenZumStuhlUndUrin'] = new fragenZumStuhlUndUrin();
