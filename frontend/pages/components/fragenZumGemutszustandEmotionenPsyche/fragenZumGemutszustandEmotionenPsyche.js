function fragenZumGemutszustandEmotionenPsyche() {}

fragenZumGemutszustandEmotionenPsyche.prototype = {
    init: function () {},
    ready: function () {
        goc.flydb.interface.get('FRAGEN_ZUM_GEMUTSZUSTAND_EMOTIONEN_PSYCHE').byValue('patientId', fragenZumGemutszustandEmotionenPsyche._data.patientId).exec((res) => {
            if (res.success && res.data.length > 0) {
                fragenZumGemutszustandEmotionenPsyche.form.fragenZumGemutszustandEmotionenPsycheForm.setValues(res.data[0]);
            } else {
                fragenZumGemutszustandEmotionenPsyche.form.fragenZumGemutszustandEmotionenPsycheForm.setValues(fragenZumGemutszustandEmotionenPsyche.defaultFormValues);
            }
        });
    },
    saveOrUpdate: function (data) {
        if (!fragenZumGemutszustandEmotionenPsyche._data.patientId) {
            goc.notifications.error('Patient not selected', 'Please select a patient from patient list page.');
            return;
        }

        data.patientId = fragenZumGemutszustandEmotionenPsyche._data.patientId;
        goc.flydb.interface.saveOrUpdate('FRAGEN_ZUM_GEMUTSZUSTAND_EMOTIONEN_PSYCHE', data, (res) => {
            if (res.success) {
                goc.notifications.success('Success', 'Patient data saved successfuly.');
                goc.page.load({
                    path: `components/familienanamnese/`,
                    domId: 'mainContent',
                    data: {
                        patientId: fragenZumGemutszustandEmotionenPsyche._data.patientId
                    },
                    pageName: `familienanamnese`,
                    animation: {
                        type: 'slideDown',
                        speed: 500
                    },
                    type: "json"
                });
            }
        });
    },
    dismiss: function () {
        goc.page.load({
            path: `components/patientList/`,
            domId: 'mainContent',
            pageName: `patientList`,
            animation: {
                type: 'slideDown',
                speed: 500
            },
            type: "json"
        });
    }
}

window['fragenZumGemutszustandEmotionenPsyche'] = new fragenZumGemutszustandEmotionenPsyche();
