function medikamenteUndNahrungsergaenzungen() {}

medikamenteUndNahrungsergaenzungen.prototype = {
    init: function() {

    },
    ready: function() {
        goc.flydb.interface.get('MEDIKAMENTE_UND_NAHRUNGSERGAENZUNGEN').byValue('patientId', medikamenteUndNahrungsergaenzungen._data.patientId).exec((res) => {
            if (res.success && res.data.length > 0) {
                medikamenteUndNahrungsergaenzungen.form.medikamenteUndNahrungsergaenzungenForm.setValues(res.data[0]);
            }
        });
    },
    saveOrUpdate: function(data) {
        if (!medikamenteUndNahrungsergaenzungen._data.patientId) {
            goc.notifications.error('Patient not selected', 'Please select a patient from patient list page.');
            return;
        }

        data.patientId = medikamenteUndNahrungsergaenzungen._data.patientId;
        goc.flydb.interface.saveOrUpdate('MEDIKAMENTE_UND_NAHRUNGSERGAENZUNGEN', data, (res)=> {
            if (res.success) {
                goc.notifications.success('Success', 'Patient data saved successfuly.');
                goc.page.load({
                    path: `components/fragenZumStuhlUndUrin/`,
                    domId: 'mainContent',
                    data: {
                        patientId: medikamenteUndNahrungsergaenzungen._data.patientId
                    },
                    pageName: `fragenZumStuhlUndUrin`,
                    animation: {
                        type: 'slideDown',
                        speed: 500
                    },
                    type: "json"
                });
            }
        });
    },
    dismiss: function() {
        goc.page.load({
            path: `components/patientList/`,
            domId: 'mainContent',
            pageName: `patientList`,
            animation: {
                type: 'slideDown',
                speed: 500
            },
            type: "json"
        });
    }
}

window['medikamenteUndNahrungsergaenzungen'] = new medikamenteUndNahrungsergaenzungen();
