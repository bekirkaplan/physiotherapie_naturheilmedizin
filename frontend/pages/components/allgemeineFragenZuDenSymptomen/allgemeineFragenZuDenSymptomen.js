function allgemeineFragenZuDenSymptomen() {}

allgemeineFragenZuDenSymptomen.prototype = {
    init: function() {

    },
    ready: function() {
        goc.flydb.interface.get('ALLGEMEINE_FRAGE_SYMPTOMEN').byValue('patientId', allgemeineFragenZuDenSymptomen._data.patientId).exec((res) => {
            if (res.success && res.data.length > 0) {
                allgemeineFragenZuDenSymptomen.form.allgemeineFragenZuDenSymptomenForm.setValues(res.data[0]);
            }
        });
    },
    saveOrUpdate: function(data) {
        if (!allgemeineFragenZuDenSymptomen._data.patientId) {
            goc.notifications.error('Patient not selected', 'Please select a patient from patient list page.');
            return;
        }

        data.patientId = allgemeineFragenZuDenSymptomen._data.patientId;
        goc.flydb.interface.saveOrUpdate('ALLGEMEINE_FRAGE_SYMPTOMEN', data, (res)=> {
            if (res.success) {
                goc.notifications.success('Success', 'Patient data saved successfuly.');
                goc.page.load({
                    path: `components/operationen/`,
                    domId: 'mainContent',
                    data: {
                        patientId: allgemeineFragenZuDenSymptomen._data.patientId
                    },
                    pageName: `operationen`,
                    animation: {
                        type: 'slideDown',
                        speed: 500
                    },
                    type: "json"
                });
            }
        });
    },
    dismiss: function() {
        goc.page.load({
            path: `components/patientList/`,
            domId: 'mainContent',
            pageName: `patientList`,
            animation: {
                type: 'slideDown',
                speed: 500
            },
            type: "json"
        });
    }
}

window['allgemeineFragenZuDenSymptomen'] = new allgemeineFragenZuDenSymptomen();
