function operationen() {
    this.addOperationenArr = [];
}

operationen.prototype = {
    init: function () {
    },
    ready: function () {
    },
    saveOrUpdate: function (data) {
        if (!operationen._data.patientId) {
            goc.notifications.error('Patient not selected', 'Please select a patient from patient list page.');
            return;
        }

        data.patientId = operationen._data.patientId;
        return {
            success: true,
            data: data
        };
    },
    dismiss: function () {
        goc.page.load({
            path: `components/patientList/`,
            domId: 'mainContent',
            pageName: `patientList`,
            animation: {
                type: 'slideDown',
                speed: 500
            },
            type: "json"
        });
    },
    nextPage: function() {
        goc.page.load({
            path: `components/medikamenteUndNahrungsergaenzungen/`,
            domId: 'mainContent',
            data: {
                patientId: operationen._data.patientId
            },
            pageName: `medikamenteUndNahrungsergaenzungen`,
            animation: {
                type: 'slideDown',
                speed: 500
            },
            type: "json"
        });
    }

}

window['operationen'] = new operationen();
