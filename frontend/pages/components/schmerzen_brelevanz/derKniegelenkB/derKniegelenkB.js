function derKniegelenkB() {}

derKniegelenkB.prototype = {
    init: function() {},
    ready: function() {
        goc.flydb.interface.get('B_RELEVANZ_derKniegelenkB').byValue('patientId', schmerzen_brelevanz._data.patientId).exec((res) => {
            if (res.success && res.data.length > 0) {
                derKniegelenkB.form.derKniegelenkBForm.setValues(res.data[0]);
            }
        });
    },
    saveOrUpdate: function(data) {
        schmerzen_brelevanz.saveOrUpdate(data, function() {
            schmerzen_arelevanz.loadForm('derFussOderZehengelenkeB');
        });
    },
    cancel: function() {
        document.getElementById('schmerzenLoadContainer').innerHTML = "";
    }
}

window['derKniegelenkB'] = new derKniegelenkB();
