function derSchultergelenkeB() {}

derSchultergelenkeB.prototype = {
    init: function() {

    },
    ready: function() {
        goc.flydb.interface.get('B_RELEVANZ_derSchultergelenke').byValue('patientId', schmerzen_brelevanz._data.patientId).exec((res) => {
            if (res.success && res.data.length > 0) {
                derSchultergelenkeB.form.derSchultergelenkeForm.setValues(res.data[0]);
            }
        });
    },
    saveOrUpdate: function(data) {
        schmerzen_brelevanz.saveOrUpdate(data, function() {
            schmerzen_arelevanz.loadForm('desEllenbogensB');
        });
    },
    cancel: function() {
        document.getElementById('schmerzenLoadContainer').innerHTML = "";
    }
}

window['derSchultergelenkeB'] = new derSchultergelenkeB();
