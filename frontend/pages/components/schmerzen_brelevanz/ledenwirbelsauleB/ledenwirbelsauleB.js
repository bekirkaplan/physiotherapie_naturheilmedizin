function ledenwirbelsauleB() {}

ledenwirbelsauleB.prototype = {
    init: function() {

    },
    ready: function() {
        goc.flydb.interface.get('B_RELEVANZ_ledenwirbelsauleB').byValue('patientId', schmerzen_brelevanz._data.patientId).exec((res) => {
            if (res.success && res.data.length > 0) {
                ledenwirbelsauleB.form.ledenwirbelsauleBForm.setValues(res.data[0]);
            }
        });
    },
    saveOrUpdate: function(data) {
        schmerzen_brelevanz.saveOrUpdate(data, function() {
            schmerzen_arelevanz.loadForm('derBrustwirbelsauleB');
        });
    },
    cancel: function() {
        document.getElementById('schmerzenLoadContainer').innerHTML = "";
    }
}

window['ledenwirbelsauleB'] = new ledenwirbelsauleB();
