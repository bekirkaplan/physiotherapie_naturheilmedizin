function derHuftgelenkeB() {}

derHuftgelenkeB.prototype = {
    init: function() {},
    ready: function() {
        goc.flydb.interface.get('B_RELEVANZ_derHuftgelenkeB').byValue('patientId', schmerzen_brelevanz._data.patientId).exec((res) => {
            if (res.success && res.data.length > 0) {
                derHuftgelenkeB.form.derHuftgelenkeBForm.setValues(res.data[0]);
            }
        });
    },
    saveOrUpdate: function(data) {
        schmerzen_brelevanz.saveOrUpdate(data, function() {
            schmerzen_arelevanz.loadForm('derKniegelenkB');
        });
    },
    cancel: function() {
        document.getElementById('schmerzenLoadContainer').innerHTML = "";
    }
}

window['derHuftgelenkeB'] = new derHuftgelenkeB();
