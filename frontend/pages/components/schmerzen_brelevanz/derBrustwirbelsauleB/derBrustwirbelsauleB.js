function derBrustwirbelsauleB() {}

derBrustwirbelsauleB.prototype = {
    init: function() {

    },
    ready: function() {
        goc.flydb.interface.get('B_RELEVANZ_derBrustwirbelsauleB').byValue('patientId', schmerzen_brelevanz._data.patientId).exec((res) => {
            if (res.success && res.data.length > 0) {
                derBrustwirbelsauleB.form.derBrustwirbelsauleBForm.setValues(res.data[0]);
            }
        });
    },
    saveOrUpdate: function(data) {
        schmerzen_brelevanz.saveOrUpdate(data, function() {
            schmerzen_arelevanz.loadForm('derHalswirbelsaeuleNackenB');
        });
    },
    cancel: function() {
        document.getElementById('schmerzenLoadContainer').innerHTML = "";
    }
}

window['derBrustwirbelsauleB'] = new derBrustwirbelsauleB();
