function schmerzen_brelevanz() {
    goc.page._loadCss('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css', 'faCssId');
    this.defaultFormValues = {};
    this.formData = null;
    this.bRelevanz = null;
}

schmerzen_brelevanz.prototype = {
    // necessary
    init: function() {
        if(! document.getElementById('video-wrap')) {
            const style = `
            .youtube-vid {
                background: black;
                margin-bottom: 1rem;
                padding: 1rem;
                border: solid 1rem #337ab7;
            }
            
            .video-container {
              position: relative;
              overflow: hidden;
              height: 0;
              padding-bottom: 56.25%; /* creates a 16:9 aspect ratio */
            }
            
            .video-container iframe,
            .video-container embed {
              position: absolute;
              top: 0;
              left: 0;
              width: 100%;
              height: 100%;
              max-width: 100%;
            }
            
            /* And set the max-width of the parent element */
            .video-wrap {
              width: 100%;
              max-width: 600px;
              margin: auto;
            }`;
            var styleSheet = document.createElement("style");
            styleSheet.innerText = style;
            styleSheet.id = 'video-wrap'
            document.head.appendChild(styleSheet);
        }
    },
    // necessary
    ready: function() {

        let yt = document.getElementById('video2');
        yt.innerHTML = `<iframe
        width='100%'
        height='500'
        src='https://www.youtube.com/embed/p8EEI9pF1QI'
        frameborder='0'
        allow='autoplay;
        encrypted-media'
        allowfullscreen></iframe>`

    },
    radioChange: function(value) {
        if (!schmerzen_brelevanz._data.patientId) {
            goc.notifications.error('Patient not selected', 'Please select a patient from patient list page.');
            return;
        }
        schmerzen_brelevanz.bRelevanz = value;
        schmerzen_brelevanz.loadForm(value);
    },
    cancel: function() {
        schmerzen_brelevanz._data.patientId = null;
        goc.page.load({
            path: 'components/patientList/',
            domId: 'mainContent',
            pageName: 'patientList',
            animation: {
                type: 'slideDown',
                speed: 500
            },
            type: "json"
        });
    },
    loadForm: function(formName){
        document.getElementById('schmerzenLoadContainer').innerHTML = '';
        goc.page.load({
            path: `components/schmerzen_brelevanz/${formName}/`,
            domId: 'schmerzenLoadContainer',
            pageName: `${formName}`,
            animation: {
                type: 'slideDown',
                speed: 500
            },
            type: "json",
            callback: function() {
                setTimeout(function() {
                    goc.page.jump(formName);
                },0);
            }
        });
    },
    saveOrUpdate: function(data, callback) {
        if (!schmerzen_brelevanz._data.patientId) {
            goc.notifications.error('Patient not selected', 'Please select a patient from patient list page.');
            return;
        }
        if (!schmerzen_brelevanz.bRelevanz) return;

        data.patientId = schmerzen_brelevanz._data.patientId;
        goc.flydb.interface.saveOrUpdate('B_RELEVANZ_' + schmerzen_brelevanz.bRelevanz, data, (res)=> {
            if (res.success) {
                goc.notifications.success('Success', 'Patient data saved successfuly.');
                if(callback) {
                    callback();
                }
            }
        });
        /*
        return {
            success: true,
            data: data,
            callback: schmerzen_brelevanz.cancel
        };*/

    }
}
window['schmerzen_brelevanz'] = new schmerzen_brelevanz();
