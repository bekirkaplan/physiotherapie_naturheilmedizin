function derFussOderZehengelenkeB() {}

derFussOderZehengelenkeB.prototype = {
    init: function() {},
    ready: function() {
        goc.flydb.interface.get('B_RELEVANZ_derFussOderZehengelenkeB').byValue('patientId', schmerzen_brelevanz._data.patientId).exec((res) => {
            if (res.success && res.data.length > 0) {
                derFussOderZehengelenkeB.form.derFussOderZehengelenkeBForm.setValues(res.data[0]);
            }
        });
    },
    saveOrUpdate: function(data) {
        schmerzen_brelevanz.saveOrUpdate(data, function() {
            goc.page.load({
                path: `components/erkrankungenSymptome/`,
                domId: 'mainContent',
                pageName: `erkrankungenSymptome`,
                data: {
                    patientId: schmerzen_brelevanz._data.patientId
                },
                animation: {
                    type: 'slideDown',
                    speed: 500
                },
                type: "json"
            });
        });
    },
    cancel: function() {
        document.getElementById('schmerzenLoadContainer').innerHTML = "";
    }
}

window['derFussOderZehengelenkeB'] = new derFussOderZehengelenkeB();
