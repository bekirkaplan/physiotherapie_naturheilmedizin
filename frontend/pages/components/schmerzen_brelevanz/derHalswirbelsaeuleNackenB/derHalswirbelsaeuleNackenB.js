function derHalswirbelsaeuleNackenB() {}

derHalswirbelsaeuleNackenB.prototype = {
    init: function() {

    },
    ready: function() {
        goc.flydb.interface.get('B_RELEVANZ_derHalswirbelsaeuleNackenB').byValue('patientId', schmerzen_brelevanz._data.patientId).exec((res) => {
            if (res.success && res.data.length > 0) {
                derHalswirbelsaeuleNackenB.form.derHalswirbelsaeuleNackenBForm.setValues(res.data[0]);
            }
        });
    },
    saveOrUpdate: function(data) {
        schmerzen_brelevanz.saveOrUpdate(data, function() {
            schmerzen_arelevanz.loadForm('derSchultergelenkeB');
        });
    },
    cancel: function() {
        document.getElementById('schmerzenLoadContainer').innerHTML = "";
    }
}

window['derHalswirbelsaeuleNackenB'] = new derHalswirbelsaeuleNackenB();
