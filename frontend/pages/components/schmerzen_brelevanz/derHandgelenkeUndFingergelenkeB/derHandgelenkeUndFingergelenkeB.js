function derHandgelenkeUndFingergelenkeB() {}

derHandgelenkeUndFingergelenkeB.prototype = {
    init: function() {

    },
    ready: function() {
        goc.flydb.interface.get('B_RELEVANZ_derHandgelenkeUndFingergelenkeB').byValue('patientId', schmerzen_brelevanz._data.patientId).exec((res) => {
            if (res.success && res.data.length > 0) {
                derHandgelenkeUndFingergelenkeB.form.derHandgelenkeUndFingergelenkeBForm.setValues(res.data[0]);
            }
        });
    },
    saveOrUpdate: function(data) {
        schmerzen_brelevanz.saveOrUpdate(data, function() {
            schmerzen_arelevanz.loadForm('derHuftgelenkeB');
        });
    },
    cancel: function() {
        document.getElementById('schmerzenLoadContainer').innerHTML = "";
    }
}

window['derHandgelenkeUndFingergelenkeB'] = new derHandgelenkeUndFingergelenkeB();
