/**
 * Created by SHOGUN on 11/11/2018.
 */
function addPatient() {
  goc.page._loadScript("https://cdnjs.cloudflare.com/ajax/libs/echarts/5.0.1/echarts.min.js",
      'echart', function() {
        goc.page._loadScript("https://cdn.jsdelivr.net/npm/echarts-gl/dist/echarts-gl.min.js", 'echart-gl');
        goc.page._loadScript("https://cdn.jsdelivr.net/npm/echarts-stat/dist/ecStat.min.js", 'echart-stat');
      });
  goc.page._loadScript('https://cdnjs.cloudflare.com/ajax/libs/cytoscape/3.17.1/cytoscape.min.js', 'cytoscope');
}
addPatient.prototype = {
  init: function() {
    console.log("addPatient loaded...");
    this.defaultFormValues = {
      "id": "",
      "personalien": {
        "name": "",
        "vorname": "",
        "geburtsdatum": "",
        "korpergewicht": "",
        "korpergewichtUnknown": true,
        "korpergrobe": "",
        "korpergrobeUnknown": true,
        "vucutKitleIndeksi": ""
      }
    };
  },
  clearForm: function() {
    addPatient.form.patientInfo.setValues(addPatient.defaultFormValues);
    addPatient.drawChart();
  },
  _boyKutleHesapla: function(kilo, boy, result) {
    let items = addPatient.form.patientInfo.items;
    let kiloVal = 0;
    let boyVal = 0;
    kiloVal = parseInt(kilo.val()) || 0;
    boyVal = parseFloat(boy.val()) || 0;

    if (boyVal !== 0 && kiloVal !== 0) {
      let value = parseFloat(kiloVal / ((boyVal) * (boyVal))).toFixed(2);
      result.val(value);
      addPatient.drawChart();
    } else {
      result.val("");
    }
  },

  ready: function() {
    addPatient.form.patientInfo.setValues(addPatient.defaultFormValues);
    let items = addPatient.form.patientInfo.items;

    let yt = document.getElementById('youtube');
    yt.innerHTML = `<iframe
        width='100%'
        height='460'
        src='https://www.youtube.com/embed/2jZMzkEM2BE'
        frameborder='0'
        allow='autoplay;
        encrypted-media'
        allowfullscreen></iframe>`

    if (addPatient._data.patientId !== undefined) {
      goc.flydb.interface.get("PATIENT_INFO").byId(addPatient._data.patientId, res => {
        if (res.success) {
          addPatient.form.patientInfo.setValues(res.data[0]);
          addPatient.drawChart();
        }
      });
    } else {
      addPatient.form.patientInfo.setValues(addPatient.defaultFormValues);
    }

    let vki = items.personalien.bodyMassIndex;
    let kilo = items.personalien.korpergewicht;
    let boy = items.personalien.korpergrobe;
    let onFunction = () => {
      addPatient._boyKutleHesapla(kilo, boy, vki);
    };
    kilo.on("change", onFunction);
    kilo.on("keyup", onFunction);

    boy.on("change", onFunction);
    boy.on("keyup", onFunction);

  },
  saveOrUpdate: function(data) {
    return {
      success: true,
      data: data,
      callback: function() {
        if (addPatient._data.patientId !== undefined) {
          goc.page.load({
            path: `components/schmerzen_arelevanz/`,
            domId: 'mainContent',
            pageName: `schmerzen_arelevanz`,
            data: {
              patientId: addPatient._data.patientId
            },
            animation: {
              type: 'slideDown',
              speed: 500
            },
            type: "json"
          });
        }
      }
    };
  },
  dismiss: function() {
    goc.page.load({
      path: `components/patientList/`,
      domId: 'mainContent',
      pageName: `patientList`,
      animation: {
        type: 'slideDown',
        speed: 500
      },
      type: "json"
    });
  },
  drawChart: function() {
    let items = addPatient.form.patientInfo.items;
    let vki = items.personalien.bodyMassIndex;
    var domLine = document.getElementById("chartLineContainer");
    addPatient.myChartLine = echarts.init(domLine);


    let dimensions = ["bodymass",
      "Normalgewicht (19-24,9)",
      "Übergewicht (25-29,9)",
      "Fettsucht Grad 1 (30-34,9)",
      "Fettsucht Grad 2 (35-39,9)",
      "Fettsucht Grad 3 (> 40)",
      "Ihr B.M.I."];
    let source = [{"bodymass":"Body Mass",
      "Normalgewicht (19-24,9)":"24.9",
      "Übergewicht (25-29,9)":"29.9",
      "Fettsucht Grad 1 (30-34,9)":"34.9",
      "Fettsucht Grad 2 (35-39,9)":"39.9",
      "Fettsucht Grad 3 (> 40)":"60",
      "Ihr B.M.I.": vki.val()}];
    let series = [];

    for (let i = 0; i < dimensions.length; i++) {
      if (dimensions[i] !== 'bodymass') {
        series.push({
          name: dimensions[i],
          type: 'bar',
          itemStyle: {
            normal: {
              label: {
                show: true,
                position: 'inside'
              }
            }
          }
        });
      }
    }


    optionLine = {
      legend: {
        type: 'scroll',
      },
      tooltip: {},
      height: '70%',
      dataset: {
        dimensions: dimensions,
        source: source
      },
      xAxis: {
        type: 'category',
        axisLabel: {
          rotate: 20
        }
      },
      yAxis: {},
      // Declare several bar series, each will be mapped
      // to a column of dataset.source by default.
      series: series,
      dataZoom: [{
        show: false,
        start: 100,
        end: 100
      },
        {
          type: 'inside',
          start: 0,
          end: 50
        },
        {
          show: false,
          yAxisIndex: 0,
          filterMode: 'empty',
          width: 30,
          height: '80%',
          showDataShadow: false,
          left: '93%'
        }]
    };
    if (optionLine && typeof optionLine === "object") {
      addPatient.myChartLine.setOption(optionLine, true);
    }
  }
};
window["addPatient"] = new addPatient();
