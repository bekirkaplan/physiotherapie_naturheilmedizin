function laborwerte() {}

laborwerte.prototype = {
    init: function () {

    },
    ready: function () {
        goc.flydb.interface.get('LABORWERTE').byValue('patientId', laborwerte._data.patientId).exec((res) => {
            if (res.success && res.data.length > 0) {
                laborwerte.form.laborwerteForm.setValues(res.data[0]);
            } else {
                laborwerte.form.laborwerteForm.setValues(laborwerte.defaultFormValues);
            }
        });
    },
    saveOrUpdate: function (data) {
        if (!laborwerte._data.patientId) {
            goc.notifications.error('Patient not selected', 'Please select a patient from patient list page.');
            return;
        }

        data.patientId = laborwerte._data.patientId;
        goc.flydb.interface.saveOrUpdate('LABORWERTE', data, (res) => {
            if (res.success) {
                goc.notifications.success('Success', 'Patient data saved successfuly.');
                laborwerte.dismiss();
            }
        });
    },
    dismiss: function () {
        goc.page.load({
            path: `components/patientList/`,
            domId: 'mainContent',
            pageName: `patientList`,
            animation: {
                type: 'slideDown',
                speed: 500
            },
            type: "json"
        });
    }
}

window['laborwerte'] = new laborwerte();
