function familienanamnese() {}

familienanamnese.prototype = {
    init: function () {

    },
    ready: function () {
        goc.flydb.interface.get('FAMILIENANAMNESE').byValue('patientId', familienanamnese._data.patientId).exec((res) => {
            if (res.success && res.data.length > 0) {
                familienanamnese.form.familienanamneseForm.setValues(res.data[0]);
            } else {
                familienanamnese.form.familienanamneseForm.setValues(familienanamnese.defaultFormValues);
            }
        });
    },
    saveOrUpdate: function (data) {
        if (!familienanamnese._data.patientId) {
            goc.notifications.error('Patient not selected', 'Please select a patient from patient list page.');
            return;
        }

        data.patientId = familienanamnese._data.patientId;
        goc.flydb.interface.saveOrUpdate('FAMILIENANAMNESE', data, (res) => {
            if (res.success) {
                goc.notifications.success('Success', 'Patient data saved successfuly.');
                goc.page.load({
                    path: `components/laborwerte/`,
                    domId: 'mainContent',
                    data: {
                        patientId: familienanamnese._data.patientId
                    },
                    pageName: `laborwerte`,
                    animation: {
                        type: 'slideDown',
                        speed: 500
                    },
                    type: "json"
                });
            }
        });
    },
    dismiss: function () {
        goc.page.load({
            path: `components/patientList/`,
            domId: 'mainContent',
            pageName: `patientList`,
            animation: {
                type: 'slideDown',
                speed: 500
            },
            type: "json"
        });
    }
}

window['familienanamnese'] = new familienanamnese();
