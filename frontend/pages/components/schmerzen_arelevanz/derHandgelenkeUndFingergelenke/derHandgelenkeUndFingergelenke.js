function derHandgelenkeUndFingergelenke() {}

derHandgelenkeUndFingergelenke.prototype = {
    init: function() {

    },
    ready: function() {
        goc.flydb.interface.get('A_RELEVANZ_derHandgelenkeUndFingergelenke').byValue('patientId', schmerzen_arelevanz._data.patientId).exec((res) => {
            if (res.success && res.data.length > 0) {
                derHandgelenkeUndFingergelenke.form.derHandgelenkeUndFingergelenkeForm.setValues(res.data[0]);
            }
        });
    },
    saveOrUpdate: function(data) {
        schmerzen_arelevanz.saveOrUpdate(data, function() {
            schmerzen_arelevanz.loadForm('derHuftgelenke');
        });
    },
    cancel: function() {
        document.getElementById('schmerzenLoadContainer').innerHTML = "";
    }
}

window['derHandgelenkeUndFingergelenke'] = new derHandgelenkeUndFingergelenke();
