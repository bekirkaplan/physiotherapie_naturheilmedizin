function desEllenbogens() {}

desEllenbogens.prototype = {
    init: function() {

    },
    ready: function() {
        goc.flydb.interface.get('A_RELEVANZ_desEllenbogens').byValue('patientId', schmerzen_arelevanz._data.patientId).exec((res) => {
            if (res.success && res.data.length > 0) {
                desEllenbogens.form.desEllenbogensForm.setValues(res.data[0]);
            }
        });
    },
    saveOrUpdate: function(data) {
        schmerzen_arelevanz.saveOrUpdate(data, function() {
            schmerzen_arelevanz.loadForm('derHandgelenkeUndFingergelenke');
        });
    },
    cancel: function() {
        document.getElementById('schmerzenLoadContainer').innerHTML = "";
    }
}

window['desEllenbogens'] = new desEllenbogens();
