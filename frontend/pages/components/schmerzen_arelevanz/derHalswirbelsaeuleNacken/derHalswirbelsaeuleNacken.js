function derHalswirbelsaeuleNacken() {}

derHalswirbelsaeuleNacken.prototype = {
    init: function() {

    },
    ready: function() {
        goc.flydb.interface.get('A_RELEVANZ_derHalswirbelsaeuleNacken').byValue('patientId', schmerzen_arelevanz._data.patientId).exec((res) => {
            if (res.success && res.data.length > 0) {
                derHalswirbelsaeuleNacken.form.derHalswirbelsaeuleNackenForm.setValues(res.data[0]);
            }
        });
    },
    saveOrUpdate: function(data) {
        schmerzen_arelevanz.saveOrUpdate(data, function() {
            schmerzen_arelevanz.loadForm('derSchultergelenke');
        });
    },
    cancel: function() {
        document.getElementById('schmerzenLoadContainer').innerHTML = "";
    }
}

window['derHalswirbelsaeuleNacken'] = new derHalswirbelsaeuleNacken();
