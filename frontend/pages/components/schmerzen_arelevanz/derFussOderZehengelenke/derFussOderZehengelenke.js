function derFussOderZehengelenke() {}

derFussOderZehengelenke.prototype = {
    init: function() {},
    ready: function() {
        goc.flydb.interface.get('A_RELEVANZ_derFussOderZehengelenke').byValue('patientId', schmerzen_arelevanz._data.patientId).exec((res) => {
            if (res.success && res.data.length > 0) {
                derFussOderZehengelenke.form.derFussOderZehengelenkeForm.setValues(res.data[0]);
            }
        });
    },
    saveOrUpdate: function(data) {
        schmerzen_arelevanz.saveOrUpdate(data, function() {
            goc.page.load({
                path: `components/schmerzen_brelevanz/`,
                domId: 'mainContent',
                pageName: `schmerzen_brelevanz`,
                data: {
                    patientId: schmerzen_arelevanz._data.patientId
                },
                animation: {
                    type: 'slideDown',
                    speed: 500
                },
                type: "json"
            });
        });
    },
    cancel: function() {
        document.getElementById('schmerzenLoadContainer').innerHTML = "";
    }
}

window['derFussOderZehengelenke'] = new derFussOderZehengelenke();
