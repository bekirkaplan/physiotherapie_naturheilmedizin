function derBrustwirbelsaule() {}

derBrustwirbelsaule.prototype = {
    init: function() {

    },
    ready: function() {
        goc.flydb.interface.get('A_RELEVANZ_derBrustwirbelsaule').byValue('patientId', schmerzen_arelevanz._data.patientId).exec((res) => {
            if (res.success && res.data.length > 0) {
                derBrustwirbelsaule.form.derBrustwirbelsauleForm.setValues(res.data[0]);
            }
        });
    },
    saveOrUpdate: function(data) {
        schmerzen_arelevanz.saveOrUpdate(data, function() {
            schmerzen_arelevanz.loadForm('derHalswirbelsaeuleNacken');
        });
    },
    cancel: function() {
        document.getElementById('schmerzenLoadContainer').innerHTML = "";
    }
}

window['derBrustwirbelsaule'] = new derBrustwirbelsaule();
