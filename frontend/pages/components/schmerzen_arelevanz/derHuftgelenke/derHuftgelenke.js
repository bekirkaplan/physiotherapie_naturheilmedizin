function derHuftgelenke() {}

derHuftgelenke.prototype = {
    init: function() {},
    ready: function() {
        goc.flydb.interface.get('A_RELEVANZ_derHuftgelenke').byValue('patientId', schmerzen_arelevanz._data.patientId).exec((res) => {
            if (res.success && res.data.length > 0) {
                derHuftgelenke.form.derHuftgelenkeForm.setValues(res.data[0]);
            }
        });
    },
    saveOrUpdate: function(data) {
        schmerzen_arelevanz.saveOrUpdate(data, function() {
            schmerzen_arelevanz.loadForm('derKniegelenk');
        });
    },
    cancel: function() {
        document.getElementById('schmerzenLoadContainer').innerHTML = "";
    }
}

window['derHuftgelenke'] = new derHuftgelenke();
