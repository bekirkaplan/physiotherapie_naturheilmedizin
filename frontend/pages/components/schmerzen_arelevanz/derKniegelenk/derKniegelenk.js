function derKniegelenk() {}

derKniegelenk.prototype = {
    init: function() {},
    ready: function() {
        goc.flydb.interface.get('A_RELEVANZ_derKniegelenk').byValue('patientId', schmerzen_arelevanz._data.patientId).exec((res) => {
            if (res.success && res.data.length > 0) {
                derKniegelenk.form.derKniegelenkForm.setValues(res.data[0]);
            }
        });
    },
    saveOrUpdate: function(data) {
        schmerzen_arelevanz.saveOrUpdate(data, function() {
            schmerzen_arelevanz.loadForm('derFussOderZehengelenke');
        });
    },
    cancel: function() {
        document.getElementById('schmerzenLoadContainer').innerHTML = "";
    }
}

window['derKniegelenk'] = new derKniegelenk();
