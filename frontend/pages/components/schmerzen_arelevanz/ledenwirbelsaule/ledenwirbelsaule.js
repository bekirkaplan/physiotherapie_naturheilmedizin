function ledenwirbelsaule() {}

ledenwirbelsaule.prototype = {
    init: function() {

    },
    ready: function() {
        goc.flydb.interface.get('A_RELEVANZ_ledenwirbelsaule').byValue('patientId', schmerzen_arelevanz._data.patientId).exec((res) => {
            if (res.success && res.data.length > 0) {
                ledenwirbelsaule.form.ledenwirbelsauleForm.setValues(res.data[0]);
            }
        });
    },
    saveOrUpdate: function(data) {
        schmerzen_arelevanz.saveOrUpdate(data, function() {
            schmerzen_arelevanz.loadForm('derBrustwirbelsaule');
        });
    },
    cancel: function() {
        document.getElementById('schmerzenLoadContainer').innerHTML = "";
    }
}

window['ledenwirbelsaule'] = new ledenwirbelsaule();
