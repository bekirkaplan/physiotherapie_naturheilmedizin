function derSchultergelenke() {}

derSchultergelenke.prototype = {
    init: function() {

    },
    ready: function() {
        goc.flydb.interface.get('A_RELEVANZ_derSchultergelenke').byValue('patientId', schmerzen_arelevanz._data.patientId).exec((res) => {
            if (res.success && res.data.length > 0) {
                derSchultergelenke.form.derSchultergelenkeForm.setValues(res.data[0]);
            }
        });
    },
    saveOrUpdate: function(data) {
        schmerzen_arelevanz.saveOrUpdate(data, function() {
            schmerzen_arelevanz.loadForm('desEllenbogens');
        });
    },
    cancel: function() {
        document.getElementById('schmerzenLoadContainer').innerHTML = "";
    }
}

window['derSchultergelenke'] = new derSchultergelenke();
