function ernaehrungTrinkverhaltenGenussmittel() {}

ernaehrungTrinkverhaltenGenussmittel.prototype = {
    init: function () {

    },
    ready: function () {
        goc.flydb.interface.get('ERNAEHRUNG_TRINKVERHALTEN_GENUSSMITTEL').byValue('patientId', ernaehrungTrinkverhaltenGenussmittel._data.patientId).exec((res) => {
            if (res.success && res.data.length > 0) {
                ernaehrungTrinkverhaltenGenussmittel.form.ernaehrungTrinkverhaltenGenussmittelForm.setValues(res.data[0]);
            } else {
                ernaehrungTrinkverhaltenGenussmittel.form.ernaehrungTrinkverhaltenGenussmittelForm.setValues(ernaehrungTrinkverhaltenGenussmittel.defaultFormValues);
            }
        });
    },
    saveOrUpdate: function (data) {
        if (!ernaehrungTrinkverhaltenGenussmittel._data.patientId) {
            goc.notifications.error('Patient not selected', 'Please select a patient from patient list page.');
            return;
        }

        data.patientId = ernaehrungTrinkverhaltenGenussmittel._data.patientId;
        goc.flydb.interface.saveOrUpdate('ERNAEHRUNG_TRINKVERHALTEN_GENUSSMITTEL', data, (res) => {
            if (res.success) {
                goc.notifications.success('Success', 'Patient data saved successfuly.');
                goc.page.load({
                    path: `components/fragenZumGemutszustandEmotionenPsyche/`,
                    domId: 'mainContent',
                    data: {
                        patientId: ernaehrungTrinkverhaltenGenussmittel._data.patientId
                    },
                    pageName: `fragenZumGemutszustandEmotionenPsyche`,
                    animation: {
                        type: 'slideDown',
                        speed: 500
                    },
                    type: "json"
                });
            }
        });
    },
    dismiss: function () {
        goc.page.load({
            path: `components/patientList/`,
            domId: 'mainContent',
            pageName: `patientList`,
            animation: {
                type: 'slideDown',
                speed: 500
            },
            type: "json"
        });
    }
}

window['ernaehrungTrinkverhaltenGenussmittel'] = new ernaehrungTrinkverhaltenGenussmittel();
