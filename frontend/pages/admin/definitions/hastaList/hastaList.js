/**
 * Created by SHOGUN on 11/11/2018.
 */
function hastaList() {};

hastaList.prototype = {
    init: function () {
        console.log("init...");
        goc.flydb.interface.getAll('users', function(res){
        	if(res.success) {
               hastaList.users = res.data;
        	}
        });
    },

    ready: function () {
        console.log("ready...");
    },

    kullaniciRender: function(data, type, row, meta) {
        let userId = row.userId;
        let user = '';
        if(userId !== undefined) {
            user = goc.utils.array.getByValue(hastaList.users, 'id', userId);
            if(user !== undefined) {
                return user.username;
            }
        }
    },
   fullReport: function() {
        var hastalar = [];
        var headers = {};
        function bul() {

            goc.mongodb.getReports("HASTA", [ { 
                "table": "KLINIK_DURUM", 
                "fk": "patientId" 
            },{ 
                "table": "IDAME_TEDAVI", 
                "fk": "patientId" 
            },{ 
                "table": "RADYOTERAPI", 
                "fk": "patientId" 
            },{ 
                "table": "LABORATUVAR_BULGULARI", 
                "fk": "patientId" 
            }], (res) => {
                console.log(res);

                var hastaFlatList = goc.utils.object.flattenList(res.data);
                var obj = {};
                for(var i=0; i<hastaFlatList.length; i++) {
                    headers = Object.keys(hastaFlatList[i]);
                    var x = Object.values(headers);
                    for(var z = 0; z < x.length; z++) {
                        obj[x[z]] = '';
                    }
                }
                hastaFlatList.unshift(obj);
                console.log(hastaFlatList);
                goc.aladb.dataToExcel('hastaList', hastaFlatList);

            });
        }
        bul();
    }



}
window['hastaList'] = new hastaList();