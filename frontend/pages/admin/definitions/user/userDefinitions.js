/**
 * Created by SHOGUN on 11/11/2018.
 */
function userDefinitions() {};

userDefinitions.prototype = {

	init: function () {
		console.log("userDefinitions loaded...");
        goc.flydb.interface.getAll('merkez', function(res){
			if(res.success) {
				userDefinitions.merkezler = res.data;
			}
		});
	},
	ready: function () {
        userDefinitions.form.userDefinitionsForm.items.btn_modal_btnModalForm.context.disabled = true;
	},
	onUserTableClick: function(data){
		console.log('onActivityClick');
		if(data.length > 0) {
			this.form.userDefinitionsForm.items.btn_modal_btnModalForm.context.disabled = false;
		} else {
			this.form.userDefinitionsForm.items.btn_modal_btnModalForm.context.disabled = true;
		}

		console.log(data);
	},
	merkezChange: function(data) {
		console.log(data.id + " " + data.text);
	},
	stateRenderer: function(data, type, row, meta){
		return data === "0" ? '<span style="color: red">Passive</span>' : data === "1" ? '<span style="color: green">Active</span>' : '<span style="color: orange">Pending</span>';
	}

}

window['userDefinitions'] = new userDefinitions();
