/**
 * Created by SHOGUN on 11/11/2018.
 */
function doktorDegis() {
	this.form = {};
	this.table = {};
	this.html = {};
	this.modal = {};
};
doktorDegis.prototype = {
	init: function () {
		console.log("userDefinitions loaded...");

	},
	ready: function () {
        doktorDegis.form.doktorDegisForm.setValues(hastaList.table.hastaListesi.getSelections()[0]);
	},
	submitBtnClick: function () {
		let items = doktorDegis.form.doktorDegisForm.items;
			var values = doktorDegis.form.doktorDegisForm.getValues();
        	delete values.hastaBilgisi;

			return {success: true, data: values, callback:doktorDegis.cancel1};
			goc.notifications.warning("{{lang-bind:passwords_do_not_match}}", "{{lang-bind:please_enter_same_passwords}}");
	},

    cancel1: function() {
        $('#modal_doktorDegisDefinition').modal('toggle');
        goc.flydb.interface.getAll('HASTA', function(res){
            if(res.success) {
			hastaList.table.hastaListesi.loadData(res.data);
            }
        });
        return true;
    }

}
window['doktorDegis'] = new doktorDegis();