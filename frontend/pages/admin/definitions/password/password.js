/**
 * Created by SHOGUN on 11/11/2018.
 */
function password() {
	this.form = {};
	this.table = {};
	this.html = {};
	this.modal = {};
};
password.prototype = {
	init: function () {
		console.log("userDefinitions loaded...");
	},
	ready: function () {
		let userId = userDefinitions.table.usersTable.getSelections()[0].id;
		password.form.passwordForm.setValues({id: userId});
	},
	submitBtnClick: function () {
		let items = password.form.passwordForm.items;
		if(items.password.value === items.repassword.value) {
			var values = password.form.passwordForm.getValues();
			delete values.repassword;
			return {success: true, data: values};
		}
		goc.notifications.warning("{{lang-bind:passwords_do_not_match}}", "{{lang-bind:please_enter_same_passwords}}");
		return false;
	}

}
window['password'] = new password();