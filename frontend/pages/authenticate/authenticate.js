function authenticate() {
}

authenticate.prototype = {
    init: function() {
        console.log("Login class init fiered");
        $('body').addClass('login');
    },
    ready: function() {
        console.log("Login class ready fiered");
    },
    getUserData: function() {
    },
    login:function () {
        $('#login_form').show();
        $('#reset_form').hide();
        $('#register_form').hide();
    },
    reset: function() {
        $('#login_form').hide();
        $('#reset_form').show();
        $('#register_form').hide();
    },
    register: function() {
        $('#login_form').hide();
        $('#reset_form').hide();
        $('#register_form').show();
    },
    validateEmail: function (email) {
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    },
    getSifremiUnuttum:function () {

        var username = $('#usernameReset').val();
        $.ajax({
            type: "POST",
            url: `${adenJsProp.mongo.url}/forgotpassword`,
            data: { mailadress: username }
        }).done(function(response) {
            if(response.success) {
                goc.notifications.success("", response.data);
            } else {
                goc.notifications.error("Mongo Get Error", response.message);
            }
        }).fail(function(xhr, status, error) {
        });

    },
    registration: function() {
        let formData = goc.form.getForm(document.getElementById('register')).getValues();
        formData.projectName = "localhost";
        if(!this.validateEmail(formData.mailadress)) {
            goc.notifications.error("Form Error", "E-mail address is not valid...");
            return;
        }
        if(formData.password === 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855' || formData.password !== formData.repassword) {
            goc.notifications.error("Form Error", "Passwords are not equals or Empty!!.");
            return;
        }

        $.ajax({
            type: "POST",
            url: `${adenJsProp.mongo.url}/registration`,
            data: formData
        }).done(function(response) {
            if(response.success) {
                goc.notifications.success("Registered", response.data);
            } else {
                goc.notifications.error("Error", response.message);
            }
        }).fail(function(xhr, status, error) {
            console.info(xhr, status, error);
        });

    }
}

window['authenticate'] = new authenticate();
