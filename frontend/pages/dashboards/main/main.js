/**
 * Created by SHOGUN on 11/11/2018.
 */
function main(){
	goc.page.load({
		path:'components/patientList/',
		domId:'mainContent',
		pageName: 'patientList',
		type: "json"
	});	
}
main.prototype = {
	init: function() {
        $('body').removeClass('login');
	},
	ready: function() {

	}
}
goc.oset('pages.main', main);