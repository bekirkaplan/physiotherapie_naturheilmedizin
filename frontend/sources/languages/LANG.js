/**
 * Created by SHOGUN on 11/11/2018.
 */
function LANG() {
	this.paths = ['lang.de', 'lang.tr', 'lang.en'];
	this.langList = [];
	this.selectedLang = 'de';
	this.data = {};
	this.state = 'main_content';
};
LANG.prototype = {

	init: function () {
		for(let i = 0; i < this.paths.length; i += 1) {
			$.getJSON('./sources/languages/' + this.paths[i] + '.json', (json) => {
				this.langList.push(json.lang);
				this.data[json.lang] = json.content;
			});
		}
	},
	ready: function() {
		console.log("LANG.js is ready....");
	},
	setSelectedLang: function(lang) {
		this.selectedLang = lang;
		this.load();
	},
	getSelectedLang: function() {
		return this.selectedLang;
	},
	getData: function() {
		return this.data[this.selectedLang];
	},
	load: function() {
		if(this.langList.indexOf(this.selectedLang) >= 0) {
			this.loader();
		}
	},
	findDataInKey : function(key, data){
		let originalKey = key;
		let keyArr = key.split(".");

		var path = "";
		var i = 0;
		for (i in keyArr) {
			path += "['" + keyArr[i] + "']"
		}

		var value = null;
		try {
			value = eval("x=data" + path);
			if(value === undefined) {
				value = originalKey;
			}
		} catch (e) {
			value = "__________";
		}
		return value;
	},
	replacer: function(string) {
		if(string === undefined) {
			string = $('#' + this.state).html();
		}

		if(string !== null) {
			var elemsInner = string.match(/{{(.*?)lang-bind(.*?)}}/g);
			if (elemsInner != null) {
				var count = elemsInner.length;
				$.each(elemsInner, (index, item) => {

					var orginalItem = item;

					if(item !== undefined){
						var pureitem = item.replace(/\s+/g, '');
						var bindString = pureitem.substring(pureitem.indexOf(":") + 1, pureitem.length - 2);
						var bindValue = bindString.split(':')[0];
						var convertHtml = bindString.split(':')[1];
					}

					var key = bindValue.split("<")[0];
					var value = this.findDataInKey(key, this.getData());

					if(value) {
						if(value.constructor.name != "Array") {
							if(convertHtml === undefined || convertHtml === "true") {
								string = string.replace(orginalItem, `<span class="lang-span" lang-bind="${key}">${value}</span>`);
							} else if(convertHtml === undefined || convertHtml === "false") {
								string = string.replace(orginalItem, `${value}`);
							}

						} else {
							var elemsFilterList = bindValue.split("<")[1];
							dataTable = goc.form.helpers.getTrList(value, eval("x=" + elemsFilterList));
							string = string.replace(orginalItem, dataTable);
						}
					}
				});
			}
			return string;
		}

	},
	loader: function() {

		var el = $("#" + this.state).find('[lang-bind]');


		$.each(el, (i, item) => {

			var bindValue = item.getAttribute("lang-bind");
			bindValue = bindValue.replace(/\s+/g, '');

			var key = bindValue.split("<")[0];
			var value = this.findDataInKey(key, this.getData());


			if(item.hasAttribute('data-toggle')) {
				item.setAttribute('data-original-title', value);
			} else {
				if (value !== undefined && value != null){
					if (value.constructor.name != "Array"){
						$(item).html(value);
					} else {
						$(item).html('');
						var elemsFilterList = bindValue.split("<")[1];
						dataTable = goc.form.helpers.getTrList(value, eval("x=" + elemsFilterList));
						$(item).html(dataTable);
					}
				}
			}


			if (!--el.length){
				this.replacer();
			}

		});
		if(el.length == 0){
			this.replacer();
		}

	}

}
goc.setProp('lang', new LANG());
