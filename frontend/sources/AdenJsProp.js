function AdenJsProp(){
    this.mode = 'production'; // development, production
    this.mongo = {
        url: "http://localhost:9097"
    };

    // FLY_DB, COUCH_DB, MONGO_DB...
    this.interceptors = {
        defaultDb: "MONGO_DB"
    }

    // justUser for users can only gets own records from database
    this.userProp = {
        justUser : true
    }
}

window['adenJsProp'] = new AdenJsProp();
